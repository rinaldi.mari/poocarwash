<div class="navbar-inner">
    <div class="collapse navbar-collapse" id="sidenav-collapse-main">

        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link active" href="#dashboard/dashboard">
                    <i class="ni ni-tv-2 text-primary"></i>
                    <span class="nav-link-text">Dashboard</span>
                </a>
            </li>
            <li class="dropdown">
                <a class="nav-link  dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                    <i class="ni ni-planet text-orange"></i>
                    <span class="nav-label">Cadastro</span>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" style="margin-left: 20%;min-width: 0em;box-shadow: none;">
                    <li><a href="#cadastro/cliente/pesquisa">Cliente</a></li>
                    <li><a href="#cadastro/carro/lista">Carro</a></li>
                    <!-- <li><a href="#">Funcionario</a></li> -->
                    <li><a href="#cadastro/tipo_servico/pesquisar">Tipo Serviço</a></li>
                </ul>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#servico/page/listagem">
                    <i class="ni ni-key-25 text-info"></i>
                    <span class="nav-link-text">Servico</span>
                </a>
            </li>
        </ul>
    </div>
</div>