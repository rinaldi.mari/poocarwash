<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main" aria-labelledby="nav1">
    <div class="scrollbar-inner">
        <?php
        // require_once("components/logo.php");
        require_once("components/side_bar.php");
        ?>
    </div>
</nav>

<div class="main-content" id="panel">
    <nav class="navbar navbar-top navbar-expand navbar-dark bg-primary border-bottom" aria-labelledby="nav2">
        <div class="container-fluid">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <?php require_once("components/nav_bar.php") ?>
            </div>
        </div>
    </nav>

    <div class="container-fluid" id="container-fluid">
        <div id="retorno"></div>
        <?php
        require_once("pages/dashboard/dashboard.php");

        require_once("components/footer.php");
        ?>
    </div>
</div>