<?php

namespace servico\model;

use classes\{Carro, Funcionario, TipoServico, Servico, StatusServico};

class Model
{
    public function getTipoServicos(): array
    {
        return (new TipoServico)->find("ID, DESCRICAO");
    }

    public function getServicos(Servico $servico): array
    {
        return $servico
            ->select("TPS.LAVADOR, TPS.GERENTE, TPS.DATA_HORA, TPS.ID,
                TPS.DESCRICAO AS DS_TIPO_SERVICO, TPS.VALOR, CAR.PLACA, STS.DESCRICAO AS DS_STATUS", "SERV")
            ->join("LEFT", "tipo_servico TPS", "TPS.ID = SERV.TIPO_SERVICO_ID")
            ->join("LEFT", "status_servico STS", "STS.ID = SERV.TIPO_SERVICO_ID")
            ->join("LEFT", "carro CAR", "CAR.ID = SERV.CARRO_ID")
            ->fetchAll();
    }

    public function getFuncionarios(string $tipo): array
    {
        return (new Funcionario)
            ->select("ID, NOME")
            ->where("AND TIPO = '{$tipo}'")
            ->orderBy("NOME")
            ->fetchAll();
    }

    public function getStatus(): array
    {
        return (new StatusServico)
            ->select("ID, DESCRICAO")
            ->orderBy("DESCRICAO")
            ->fetchAll();
    }

    public function getPlcasCarro(): array
    {
        return (new Carro)
            ->select("ID, PLACA")
            ->orderBy("PLACA")
            ->fetchAll();
    }

    public function novoServico(Servico $servico): bool
    {
        return $servico->create();
    }

    public function atualizarServico(Servico $servico): bool
    {
        $servico->setAllData();
        $servico->setDataHora((new \DateTime())->format("d/m/Y"));

        return $servico
            ->update()
            ->where("AND ID = :ID")
            ->binds([":ID" => $servico->getId()])
            ->execute();
    }

    public function apagarServico(Servico $servico): bool
    {
        return $servico
            ->delete()
            ->where("AND ID = :ID")
            ->binds([":ID" => $servico->getId()])
            ->execute();
    }
}
