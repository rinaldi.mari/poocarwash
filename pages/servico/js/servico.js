function getServicos(from) {
    $.ajax({
        method: "GET",
        url: "pages/servico/controller/Controller.php",
        data: $("#" + from).serializeArray(),
        async: true,
        dataType: "JSON",
    })
        .done(function (retorno) {
            document.getElementById("retorno").innerHTML = retorno
        })
        .fail(function (XMLHttpRequest, textStatus, errorThrown) {
            alert("Erro ao bucar os serviços")
        })
}

function novoServico(from) {
    $.ajax({
        method: "POST",
        url: "pages/servico/controller/Controller.php",
        data: $("#" + from).serializeArray(),
        async: true,
        dataType: "JSON",
    })
        .done(function (retorno) {
            document.getElementById("retorno").innerHTML = retorno
        })
        .fail(function (XMLHttpRequest, textStatus, errorThrown) {
            alert("Erro ao bucar os serviços")
        })
}

function carregarCombos() {
    comboPlacaCarro()
    comboTipoServico()
    comboGerente()
    comboFuncionario()
    comboStatus()
}

function comboTipoServico() {
    $.ajax({
        method: "GET",
        url: "pages/servico/controller/Controller.php",
        data: "acao=comboTipoServico",
        async: true,
        dataType: "JSON",
    })
        .done(function (retorno) {
            let options = "<option value=''>Selecione...</option>"

            $.each(retorno, function (index, value) {
                options += `<option value='${value.ID}'>${value.DESCRICAO}</option>`
            })

            $("#id").html(options)
        })
        .fail(function (XMLHttpRequest, textStatus, errorThrown) {
            alert("Erro ao bucar os serviços")
        })
}

function comboPlacaCarro() {
    $.ajax({
        method: "GET",
        url: "pages/servico/controller/Controller.php",
        data: "acao=comboPlacaCarro",
        async: true,
        dataType: "JSON",
    })
        .done(function (retorno) {
            let options = "<option value=''>Selecione...</option>"

            $.each(retorno, function (index, value) {
                options += `<option value='${value.ID}'>${value.PLACA}</option>`
            })

            $("#carro").html(options)
        })
        .fail(function (XMLHttpRequest, textStatus, errorThrown) {
            alert("Erro ao bucar os serviços")
        })
}

function comboGerente() {
    $.ajax({
        method: "GET",
        url: "pages/servico/controller/Controller.php",
        data: "acao=comboGerente",
        async: true,
        dataType: "JSON",
    })
        .done(function (retorno) {
            let options = "<option value=''>Selecione...</option>"

            $.each(retorno, function (index, value) {
                options += `<option value='${value.ID}'>${value.NAME}</option>`
            })

            $("#lavador").html(options)
        })
        .fail(function (XMLHttpRequest, textStatus, errorThrown) {
            alert("Erro ao bucar os serviços")
        })
}

function comboFuncionario() {
    $.ajax({
        method: "GET",
        url: "pages/servico/controller/Controller.php",
        data: "acao=comboFuncionario",
        async: true,
        dataType: "JSON",
    })
        .done(function (retorno) {
            let options = "<option value=''>Selecione...</option>"

            $.each(retorno, function (index, value) {
                options += `<option value='${value.ID}'>${value.NAME}</option>`
            })

            $("#gerente").html(options)
        })
        .fail(function (XMLHttpRequest, textStatus, errorThrown) {
            alert("Erro ao bucar os serviços")
        })
}

function comboStatus() {
    $.ajax({
        method: "GET",
        url: "pages/servico/controller/Controller.php",
        data: "acao=comboStatus",
        async: true,
        dataType: "JSON",
    })
        .done(function (retorno) {
            let options = "<option value=''>Selecione...</option>"

            $.each(retorno, function (index, value) {
                options += `<option value='${value.ID}'>${value.DESCRICAO}</option>`
            })

            $("#status").html(options)
        })
        .fail(function (XMLHttpRequest, textStatus, errorThrown) {
            alert("Erro ao bucar os serviços")
        })
}

function pgNovoServico() {
    window.location.hash = "servico/page/cadastro"
}

function cancelar() {
    window.location.hash = "servico/page/listagem"
}
function pgListagem() {
    window.location.hash = "servico/page/listagem"
}
