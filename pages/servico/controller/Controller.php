<?php

namespace servico\controller;

require_once($_SERVER["DOCUMENT_ROOT"] . "/core/autoload/autoload.php");

use abstracts\ControllerAbstract;
use classes\{Carro, Servico};
use servico\{model\Model, view\View};

class Controller extends ControllerAbstract
{
    private View $view;
    private Model $model;

    public function __construct()
    {
        $this->view = new View;
        $this->model = new Model;
        parent::__construct();
    }

    protected function getServicos(): string
    {
        return $this->view->montarTabelaServico($this->model->getServicos(new Servico));
    }

    protected function comboPlacaCarro(): array
    {
        return $this->model->getPlcasCarro(new Carro);
    }

    protected function comboTipoServico(): array
    {
        return $this->model->getTipoServicos();
    }

    protected function comboGerente(): array
    {
        return $this->model->getFuncionarios("Gerente");
    }

    protected function comboFuncionario(): array
    {
        return $this->model->getFuncionarios("Funcionario");
    }

    protected function comboStatus(): array
    {
        return $this->model->getStatus();
    }

    protected function novoServico(): bool
    {
        $servico = new Servico;
        $servico->setAllData();
        $servico->setDataHora((new \DateTime())->format("d/m/Y H:i:s"));

        return $this->model->novoServico($servico);
    }

    protected function atualizarServico(): bool
    {
        return $this->model->apagarServico(new Servico);
    }

    protected function apagarServico(): bool
    {
        return $this->model->apagarServico(new Servico);
    }
}

Controller::init();
