<form id="cadastrar_servicos">
    <div class="card">
        <div class="card-header">
            <h1 class="title">Cadastrar serviços</h4>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-2">
                    <label for="carro">Placa do carro</label>
                    <div class="form-group">
                        <div class="input-group-alternative">
                            <select class="form-control" data-toggle="select" title="Placa do carro" data-placeholder="Placa do carro" id="carro" name="carro">
                                <option value="0">Carregando...</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <label for="carro">Lavador</label>
                    <div class="form-group">
                        <div class="input-group-alternative">
                            <select class="form-control" data-toggle="select" title="Lavador" data-placeholder="Lavador" id="lavador" name="lavador">
                                <option value="0">Selecione...</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <label for="carro">Gerente</label>
                    <div class="form-group">
                        <div class="input-group-alternative">
                            <select class="form-control" data-toggle="select" title="Lavador" data-placeholder="Gerente" id="gerente" name="gerente">
                                <option value="0">Selecione...</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <label for="id">Tipo do Serviço</label>
                    <div class="form-group">
                        <div class="input-group-alternative">
                            <select class="form-control" data-toggle="select" title="Tipo serviço" data-placeholder="Tipo do Serviço" id="id" name="id">
                                <option value="0">Carregando...</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <label for="id">Status</label>
                    <div class="form-group">
                        <div class="input-group-alternative">
                            <select class="form-control" data-toggle="select" title="Status" data-placeholder="Status" id="status" name="status">
                                <option value="0">Carregando...</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group text-right">
                        <input type="hidden" name="acao" id="acao" value="novoServico">
                        <button type="button" class="btn btn-primary" onclick="novoServico('cadastrar_servicos')">Cadastrar</button>
                        <button type="button" class="btn btn-primary" onclick="pgListagem()">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>

<div id="retorno"></div>

<script src="pages/servico/js/servico.js"></script>
<script>
    carregarCombos()
</script>