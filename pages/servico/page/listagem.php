<form id="pesquisar_servicos">
    <div class="card">
        <div class="card-header">
            <h1 class="title">Buscar serviços</h4>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-3">
                    <label for="placa">Placa</label>
                    <div class="form-group">
                        <div class="input-group-alternative">
                            <input type="text" class="form-control" id="placa" name="placa"
                                placeholder="Placa do Carro">
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <label for="id">Tipo do Serviço</label>
                    <div class="form-group">
                        <div class="input-group-alternative">
                            <select class="form-control" data-toggle="select" title="Tipo serviço"
                                data-placeholder="Tipo do Serviço" id="id" name="id">
                                <option value="0">Carregando...</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <label for="preco">Preço R$</label>
                    <div class="form-group">
                        <div class="input-group-alternative">
                            <input type="text" class="form-control" id="preco" name="preco" placeholder="Preço"
                                value="">
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <label for="status">Status</label>
                    <div class="form-group">
                        <div class="input-group-alternative">
                            <input type="text" class="form-control" id="status" name="status"
                                placeholder="Status do Serviço" value="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <label for="data_hora">Data</label>
                    <div class="form-group">
                        <div class="input-group input-group-alternative">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                            </div>
                            <input class="form-control datepicker" id="data_hora" name="data_hora"
                                placeholder="Data do cadastro" type="text"
                                value="<?php echo (new \DateTime())->format("d/m/Y") ?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <label for="lavador">Lavador</label>
                    <div class="form-group">
                        <div class="input-group-alternative">
                            <input type="text" class="form-control" id="lavador" name="lavador"
                                placeholder="Nome do Lavador">
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <label for="gerente">Gerente</label>
                    <div class="form-group">
                        <div class="input-group-alternative">
                            <input type="text" class="form-control" id="gerente" name="gerente"
                                placeholder="Nome do Gerente">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group text-right">
                        <input type="hidden" name="acao" id="acao" value="getServicos">
                        <button type="button" class="btn btn-primary"
                            onclick="getServicos('pesquisar_servicos', 'event')">Pesquisar</button>
                        <button type="button" class="btn btn-primary" onclick="pgNovoServico()">Novo Serviço</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<div id="retorno"></div>

<script src="pages/servico/js/servico.js"></script>
<script>
comboTipoServico()
</script>