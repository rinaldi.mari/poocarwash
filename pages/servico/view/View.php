<?php

namespace servico\view;

use utils\Tabela;

class View
{
    public function montarTabelaServico(array $dados): string
    {
        $tabela = new Tabela;

        if (empty($dados)) {
            return $tabela->tabelaVazia();
        }

        $tabela->addTitulo("Lista de todos os serviços cadastrados");
        $cabecalho = ["Placa", "Serviço", "Valor R$", "Status", "Data/Hora Solic. Serviço", "Lavador", "Gerente"];

        foreach ($cabecalho as $texto) {
            $tabela->addTextoCabecalho($texto);
        }

        foreach ($dados as $coluna) {
            $tabela->addTexto($coluna["PLACA"]);
            $tabela->addTexto($coluna["DS_TIPO_SERVICO"]);
            $tabela->addTexto($coluna["VALOR"]);
            $tabela->addTexto($coluna["DS_STATUS"]);
            $tabela->addTexto($coluna["DATA_HORA"]);
            $tabela->addTexto($coluna["LAVADOR"]);
            $tabela->addTexto($coluna["GERENTE"]);

            $tabela->addLinha();
        }

        return $tabela->montarTabela();
    }
}