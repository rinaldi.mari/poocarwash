<br>
<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header">
                LISTA DE CARROS
                <a title="Novo carro" href="#cadastro/carro/form" class="btn btn-success float-right"> <i class="fa fa-plus"></i> </a>
            </div>
            <div id="table-card" class="card-body">

            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        $.ajax({
            url: 'pages/cadastro/controller/Controller.php',
            data: 'acao=getcarros',
            method: 'GET',
            dataType: 'JSON'
        }).done(function(data) {
            $("#table-card").html(data);
        }).fail(function(error) {
            alert("Erro ao obter carros");
        });
    });
</script>