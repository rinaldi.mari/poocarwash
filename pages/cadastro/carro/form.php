<br>
<div class="row">
    <div class="col-6">
        <div class="card">
            <div class="card-header bg-transparent">
                CADASTRO DE CARRO
                <a title="Voltar para lista" href="#cadastro/carro/lista" class="btn btn-success float-right"> <i class="fa fa-arrow-left"></i> </a>
            </div>
            <div class="card-body">
                <form id="cadastro-carro">

                    <div class="form-group">
                        <label for="cliente">Cliente *</label>
                        <select class="form-control form-control-sm" name="cliente" id="cliente">
                            <option value=""> -- selecione --</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="placa">Placa *</label>
                        <input class="form-control form-control-sm" type="text" name="placa" maxlength="7" id="">
                    </div>
                    <div class="form-group">
                        <label for="cor">Cor *</label>
                        <input class="form-control form-control-sm" type="text" name="cor" id="cor">
                    </div>
                    <div class="form-group">
                        <label for="marca">Marca *</label>
                        <input class="form-control form-control-sm" type="text" name="marca" id="marca">
                    </div>
                    <div class="form-group">
                        <label for="placa">Modelo *</label>
                        <input class="form-control form-control-sm" type="text" name="modelo" id="placa">
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="acao" value="novocarro">
                        <button id="salvar" type="button" class="btn btn-primary">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $("#salvar").click(function() {
            var formData = $("#cadastro-carro").serializeArray();
            // console.log(formData);
            // console.log($("#cadastro-carro").serializeArray());

            $.ajax({
                type: 'POST',
                data: formData,
                url: "pages/cadastro/controller/Controller.php",
            }).done(function(data) {
                // console.log('success');
                window.location.hash = 'cadastro/carro/lista';
            }).fail(function(error) {
                alert('Erro ao tentar cadastrar o carro');
            });
        });

        $.ajax({
            type: 'GET',
            url: "pages/cadastro/controller/Controller.php",
            data: "acao=getClientesJson",
            dataType: "JSON"
        }).done(function(data) {
            let options = "<option value=''>Selecione...</option>"

            $.each(data, function(index, value) {
                options += `<option value='${value.ID}'>${value.NOME}</option>`
            })

            $("#cliente").html(options)
        }).fail(function(error) {
            alert('Erro ao obter clientes');
        });
    });
</script>