function getClientes(form, e) {
    $.ajax({
        method: "GET",
        url: "pages/cadastro/controller/Controller.php",
        data: $("#" + form).serializeArray(),
        async: true,
        dataType: "JSON",
        success: function (retorno) {
            document.getElementById("retorno").innerHTML = retorno
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("Erro ao bucar os clientes")
        },
    })
}

function novoCliente(form) {
    $.ajax({
        method: "GET",
        url: "pages/cadastro/controller/Controller.php",
        data: $("#" + form).serializeArray(),
        async: true,
        dataType: "JSON",
        success: function (retorno) {
            alert(`${retorno}`)
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("Erro ao cadastrar o cliente")
        },
    })
}

function pgNovoCliente() {
    window.location = "#cadastro/cliente/cadastro"
}

//TipoServico
function getTipoServicos(from, e) {
    $.ajax({
        method: "GET",
        url: "pages/cadastro/controller/Controller.php",
        data: $("#" + from).serializeArray(),
        async: true,
        dataType: "JSON",
        success: function (retorno) {
            document.getElementById("retorno").innerHTML = retorno
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("Erro ao bucar os Tipo serviço")
        },
    })

    e.preventdefault()
    return false
}

function pgNovoTipoServico() {
    window.location = "#cadastro/tipo_servico/cadastro"
}

function cancelarTipoServico() {
    window.location.hash = "#cadastro/tipo_servico/pesquisar"
}