<?php

namespace cadastro\model;

use classes\{Cliente, Funcionario, Carro, PessoaHasCarro, TipoServico};
use abstracts\ModelAbstract;

class Model extends ModelAbstract
{
    public function getClientes(Cliente $cliente): array
    {
        return $cliente
            ->select("PE.NOME, PE.TELEFONE, PE.SALARIO, PE.ID, PE.EMAIL, CAR.PLACA", "PE")
            ->join("LEFT", "pessoa_has_carro PHC", "PHC.PESSOA_ID = PE.ID")
            ->join("LEFT", "carro CAR", "CAR.ID = PHC.CARRO_ID")
            ->fetchAll();
    }

    public function novoCliente(): bool
    {
        $cliente = new Cliente;

        $_REQUEST[strtolower(Cliente::TIPO)] = "Cliente";
        $_REQUEST[strtolower(Cliente::SALARIO)] = 1.1;
        $_REQUEST[strtolower(Cliente::TELEFONE)] = 111;
        $_REQUEST[strtolower(Cliente::EMAIL)] = "asdas@asdsa.com";

        $cliente->setAllData();

        return $cliente->create();
    }
    public function novoCarro(Carro $carro, $idCliente): bool
    {
        $result = FALSE;
        try {
            $this->pdo->beginTransaction();
            $sqlCarro = $this->pdo->prepare("INSERT INTO carro (placa, cor, marca, modelo)
                                 VALUES (:placa, :cor, :marca, :modelo)");
            $sqlCarro->bindValue(":placa", $carro->getPlaca());
            $sqlCarro->bindValue(":cor", $carro->getCor());
            $sqlCarro->bindValue(":marca", $carro->getMarca());
            $sqlCarro->bindValue(":modelo", $carro->getModelo());
            $sqlCarro->execute();

            $idCarro = $this->pdo->lastInsertId();
            $pessoaHasCarro = new PessoaHasCarro();
            $pessoaHasCarro->setCarroId($idCarro);
            $pessoaHasCarro->setPessoaId($idCliente);

            $sqlPessoaHasCarro = $this->pdo->prepare("INSERT INTO pessoa_has_carro (pessoa_id, carro_id)
                                                      VALUES (:pessoa_id, :carro_id)");
            $sqlPessoaHasCarro->bindValue(":pessoa_id", $pessoaHasCarro->getPessoaId());
            $sqlPessoaHasCarro->bindValue(":carro_id", $pessoaHasCarro->getCarroId());
            $sqlPessoaHasCarro->execute();

            $result = $this->pdo->commit();
        } catch (\Exception $e) {
            $this->pdo->rollBack();
        }
        return $result;
    }

    public function getCarros(Carro $carro): array
    {
        $query = $carro->select('PE.NOME AS CLIENTE, C.PLACA, C.COR, C.MARCA, C.MODELO', 'C')
            ->join('INNER', "pessoa_has_carro PHC", "PHC.CARRO_ID = C.ID")
            ->join('INNER', "pessoa PE", "PE.ID = PHC.PESSOA_ID");
        // echo $query->getFullQuery();
        return $query->fetchAll();
    }

    // public function atualizarCliente(Cliente $cliente): bool
    // {
    //     $cliente->setAllData();
    //     $cliente->setDataHora((new \DateTime())->format("YYYY-MM-DD"));

    //     return $cliente
    //         ->update()
    //         ->where("AND ID = :ID")
    //         ->binds([":ID" => $cliente->getId()])
    //         ->execute();
    // }

    // public function apagarCliente(Cliente $cliente): bool
    // {
    //     return $cliente
    //         ->delete()
    //         ->where("AND ID = :ID")
    //         ->binds([":ID" => $cliente->getId()])
    //         ->execute();
    // }

    //TipoServico
    public function novoTipoServico(TipoServico $tipoServico): bool
    {
        $tipoServico->setAllData();

        return $tipoServico->create();
    }
}
