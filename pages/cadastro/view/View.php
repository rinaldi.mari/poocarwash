<?php

namespace cadastro\view;

use utils\Tabela;

class View
{
    public function montarTabelaCliente(array $dados): string
    {

        $tabela = new Tabela;

        $tabela->addTitulo("Lista de todos os clientes cadastrados");
        $cabecalho = ["ID", "Nome", "Telefone", "Email"];

        foreach ($cabecalho as $texto) {
            $tabela->addTextoCabecalho($texto);
        }

        foreach ($dados as $coluna) {
            $tabela->addTexto($coluna["ID"]);
            $tabela->addTexto($coluna["NOME"]);
            $tabela->addTexto($coluna["TELEFONE"]);
            $tabela->addTexto($coluna["EMAIL"]);

            $tabela->addLinha();
        }

        return $tabela->montarTabela();
    }

    //TipoServico
    public function montarTabelaTipoServico(array $dados): string
    {
        $tabela = new Tabela;

        $tabela->addTitulo("Lista de todos os Tipos de serviços");
        $cabecalho = ["ID", "Descrição", "Valor"];

        foreach ($cabecalho as $texto) {
            $tabela->addTextoCabecalho($texto);
        }

        foreach ($dados as $coluna) {
            $tabela->addTexto($coluna["ID"]);
            $tabela->addTexto($coluna["DESCRICAO"]);
            $tabela->addTexto($coluna["VALOR"]);
            $tabela->addLinha();
        }

        return $tabela->montarTabela();
    }
    public function montarTabelaCarro(array $dados): string
    {
        $tabela = new Tabela;

        $tabela->addTitulo("Lista de todos os clientes cadastrados");
        $cabecalho = ["Cliente", "Placa", "Carro", "Telefone", "Email"];

        foreach ($cabecalho as $texto) {
            $tabela->addTextoCabecalho($texto);
        }

        foreach ($dados as $coluna) {
            $tabela->addTexto($coluna["CLIENTE"]);
            $tabela->addTexto($coluna["PLACA"]);
            $tabela->addTexto($coluna["COR"]);
            $tabela->addTexto($coluna["MARCA"]);
            $tabela->addTexto($coluna["MODELO"]);

            $tabela->addLinha();
        }

        return $tabela->montarTabela();
    }
}
