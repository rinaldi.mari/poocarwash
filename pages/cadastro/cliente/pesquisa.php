<form id="pesquisar_clientes">
  <div class="card">
    <div class="card-header">
      <h1 class="title">Buscar cliente</h4>
    </div>
    <div class="content">
      <div class="row">
        <div class="col-md-6">
          <label for="nome">Nome</label>
          <div class="form-group">
            <div class="input-group-alternative">
              <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome do cliente">
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <label for="carro">Carro</label>
          <div class="form-group">
            <div class="input-group-alternative">
              <input type="text" class="form-control" id="carro" name="carro" placeholder="Carro" value="">
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <label for="data_hora">Data</label>
          <div class="form-group">
            <div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
              </div>
              <input class="form-control datepicker" id="data_hora" name="data_hora" placeholder="Data do cadastro" type="text" value="<?php echo (new \DateTime())->format("d/m/Y") ?>">
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group text-right">
            <input type="hidden" name="acao" id="acao" value="getClientes">
            <button type="button" class="btn btn-primary" onclick="getClientes('pesquisar_clientes', 'event')">Pesquisar</button>
            <button type="button" class="btn btn-primary" onclick="pgNovoCliente()">Novo Cliente</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>

<div id="retorno"></div>

<script src="pages/cadastro/js/cadastro.js"></script>
