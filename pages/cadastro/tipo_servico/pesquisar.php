<form id="pesquisar_tipoServico">
  <div class="card">
    <div class="card-header">
      <h1 class="title">Buscar Tipo Serviço</h4>
    </div>
    <div class="content">
      <div class="row">
        <div class="col-md-6">
          <label for="nome">Descrição</label>
          <div class="form-group">
            <div class="input-group-alternative">
              <input type="text" class="form-control" id="descricao" name="descricao" placeholder="Descrção do Tipo Serviço">
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <label for="carro">Valor R$</label>
          <div class="form-group">
            <div class="input-group-alternative">
              <input type="text" class="form-control" id="valor" name="valor" placeholder="Preço" value="">
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group text-right">
            <input type="hidden" name="acao" id="acao" value="getTipoServicos">
            <button type="button" class="btn btn-primary" onclick="getTipoServicos('pesquisar_tipoServico', 'event')">Pesquisar</button>
            <button type="button" class="btn btn-primary" onclick="pgNovoTipoServico()">Novo Cliente</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>

<div id="retorno"></div>

<script src="pages/cadastro/js/cadastro.js"></script>
<!-- <script>
  comboTipoServico()
</script> -->