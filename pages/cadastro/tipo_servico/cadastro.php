<form id="cadastrar_tipoServico">
  <div class="card">
    <div class="card-header">
      <h1 class="title">Cadastrar Tipo Serviço</h4>
    </div>
    <div class="content">
      <div class="row">
        <div class="col-md-6">
          <label for="nome">Descrição</label>
          <div class="form-group">
            <div class="input-group-alternative">
              <input type="text" class="form-control" id="descricao" name="descricao" placeholder="Descrição Tipo Serviço">
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <label for="carro">Valor R$</label>
          <div class="form-group">
            <div class="input-group-alternative">
              <input type="text" class="form-control" id="valor" name="valor" placeholder="Preço" value="">
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group text-right">
            <input type="hidden" name="acao" id="acao" value="getTipoServico">
            <button type="button" class="btn btn-primary" onclick="cancelarTipoServico()">Cancelar</button>
            <button type="button" class="btn btn-primary" onclick="">Salvar</button>
          </div>
        </div>
      </div>
    </div>
</form>

<div id="retorno"></div>

<script src="pages/cadastro/js/cadastro.js"></script>
<!-- <script>
  comboTipoServico()
</script> -->