<?php

namespace cadastro\controller;

require_once($_SERVER["DOCUMENT_ROOT"] . "/core/autoload/autoload.php");

use abstracts\ControllerAbstract;
use classes\{Cliente, Funcionario, Carro, TipoServico};
use cadastro\{model\Model, view\View};
use servico\model\Model as ModelModel;

class Controller extends ControllerAbstract
{
    private View $view;
    private Model $model;

    public function __construct()
    {
        $this->view = new View;
        $this->model = new Model;
        parent::__construct();
    }



    protected function getClientes(): string
    {
        return $this->view->montarTabelaCliente($this->getClientesJson());
    }

    protected function getClientesJson(): array
    {
        return $this->model->getClientes(new Cliente);
    }

    protected function getCarros(): string
    {
        return $this->view->montarTabelaCarro($this->model->getCarros(new Carro));
    }

    protected function novoCliente(): bool
    {
        return $this->model->novoCliente();
    }

    //protected function atualizarCliente(): bool
    //{
    //    return $this->model->apagarCliente(new Cliente);
    //}

    //protected function apagarCliente(): bool
    //{
    //    return $this->model->apagarCliente(new Cliente);
    //}

    //TipoServico
    protected function getTipoServicos(): string
    {
        $modelServico = new ModelModel;
        return $this->view->montarTabelaTipoServico($modelServico->getTipoServicos(new TipoServico));
    }

    protected function novoCarro(): bool
    {
        $carro = new Carro();
        $carro->setAllData();
        $idCliente = $_REQUEST['cliente'];
        return $this->model->novoCarro($carro, $idCliente);
    }
}

Controller::init();
