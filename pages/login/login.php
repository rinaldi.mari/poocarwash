<div class="bg-gradient-info">
    <div class="main-content">
        <div class="header py-9">
            <div class="container">
                <div class="header-body text-center mb-7">
                    <div class="row justify-content-center">
                        <div class="col-xl-5 col-lg-6 col-md-8 px-5">
                            <h1 class="text-white"> Seja bem-bindo!</h1>
                            <p class="text-lead text-white">Efetui seu cadastro para desfrutar de nossos serviços.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container mt--8 pb-5">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-7">
                    <form id="form_login">
                        <div class="card bg-secondary border-0 mb-0">
                            <div class="card-body px-lg-5 py-lg-5">
                                <div class="form-group mb-3">
                                    <div class="input-group input-group-merge input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                        </div>
                                        <input class="form-control" placeholder="Email" type="email" name="email" id="email" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group input-group-merge input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                        </div>
                                        <input class="form-control" placeholder="Senha" type="password" name="senha" id="senha" value="">
                                    </div>
                                </div>
                                <div class="custom-control custom-control-alternative custom-checkbox">
                                    <input class="custom-control-input" id=" customCheckLogin" type="checkbox">
                                    <label class="custom-control-label" for=" customCheckLogin">
                                        <span class="text-muted">Lembrar</span>
                                    </label>
                                </div>
                                <div class="text-center">
                                    <input type="hidden" name="acao" id="acao" value="logar">
                                    <button type="button" class="btn btn-success my-3 px-8" onclick="logar('form_login')">Entrar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row mt-3">
                        <div class="col-6">
                            <button type="button" class="btn btn-primary my-2 font-weight-bold" onclick=""><small>Esqueceu a senha?</small></a>
                        </div>
                        <div class="col-6 text-right">
                            <button type="button" class="btn btn-primary my-2 font-weight-bold" onclick="telaNovoUsuario()"><small>Criar uma conta</small></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-6"></div>
    </div>

</div>

<script src="pages/login/login.js"></script>
