<?php

namespace login;

require_once($_SERVER["DOCUMENT_ROOT"] . "/core/autoload/autoload.php");

use abstracts\ControllerAbstract;
use classes\Pessoa;
use login\Model;

class Controller extends ControllerAbstract
{
    private Model $model;

    public function __construct()
    {
        $this->model = new Model;
        parent::__construct();
    }

    protected function logar()
    {
        $pessoa = new Pessoa;
        $pessoa->setEmail($_REQUEST["email"]);

        $resultado = $this->model->logar($pessoa);

        $pagina = "novo_usuario/novo_usuario";

        if (empty($resultado)) {
            $pagina = "index";
        }

        return $pagina;
    }
}

Controller::init();
