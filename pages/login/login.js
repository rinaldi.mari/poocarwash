const url_controller = "pages/login/Controller.php"
const url_novo_usuario = "novo_usuario/novo_usuario"

function logar(form) {
    $.ajax({
        method: "GET",
        url: url_controller,
        data: $("#" + form).serializeArray(),
        async: true,
        dataType: "JSON",
        success: function (retorno) {
            window.location.hash = retorno
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("Erro ao acessar plataforma")
        },
    })
}

function telaNovoUsuario() {
    window.location.hash = url_novo_usuario
}
