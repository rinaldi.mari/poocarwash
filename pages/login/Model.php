<?php

namespace login;

use classes\Pessoa;

class Model
{
    public function logar(Pessoa $pessoa)
    {
        $dados = $pessoa->select("ID, EMAIL, NOME")
            ->where("AND EMAIL = :EMAIL")
            ->binds([":" . $pessoa::EMAIL => $pessoa->getEmail()])
            ->fetch();
        return empty($dados);
    }
}
