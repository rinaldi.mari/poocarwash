<?php

namespace utils;

class Tabela
{
    private string $acao = "";
    private string $texto = "";
    private string $linha = "";
    private string $cabecalho = "";
    private string $tabela = "";
    private string $titulo = "";
    private string $tagsTabelaInicio = "<div class='card'><table class='table table-hover'>";
    private string $tagsTabelaFim = "</table></div>";

    public function addTextoCabecalho(?string $textoCabecalho, string $posicao = "text-left"): void
    {
        $this->cabecalho .= "<th class='{$posicao}'>{$textoCabecalho}</th>";
    }

    public function addAcao(?string $acao): void
    {
        $acao = $this->validarString($acao);
        $this->acao .= "<td>{$acao}</td>";
    }

    public function addTexto(?string $texto): void
    {
        $texto = $this->validarString($texto);
        $this->texto .= "<td>{$texto}</td>";
    }

    public function addLinha(): void
    {
        $this->linha .= "<tr>{$this->texto}{$this->acao}</tr>";
        $this->texto = "";
        $this->acao = "";
    }

    public function montarTabela(): string
    {
        $this->tabela = $this->montarCabecalho() . $this->montarCorpo();
        return "{$this->tagsTabelaInicio}{$this->titulo}{$this->tabela}{$this->tagsTabelaFim}";
    }

    public function addTitulo(?string $titulo): void
    {
        $this->titulo = "<div class='card-header'><h2>$titulo</h2></div>";
    }

    public function tabelaVazia(): string
    {
        return "{$this->tagsTabelaInicio}<div class='tabela-vazia text-danger text-center'>Nenhum dado encontrado!</div>{$this->tagsTabelaFim}";
    }

    private function montarCabecalho(): string
    {
        return "<thead><tr>{$this->cabecalho}</tr></thead>";
    }

    private function montarCorpo(): string
    {
        return "<tbody>{$this->linha}</tbody>";
    }

    private function validarString($dado): string
    {
        return $dado ?? "";
    }
}
