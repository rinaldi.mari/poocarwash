<?php

namespace config\connection;

require_once($_SERVER["DOCUMENT_ROOT"] . "/core/autoload/autoload.php");

use interfaces\IConnection;

abstract class MySQLConnection implements IConnection
{
    private static \PDO $conexao;

    private static string $dsn = "mysql:host=localhost;dbname=poocarwash";
    private static array  $options = [\PDO::FETCH_ASSOC];
    private static string $username = "root";
    private static string $password = "";

    public static function getInstance(): \PDO
    {
        if (!isset(self::$conexao)) {
            self::criarConexao();
        }

        return self::$conexao;
    }

    private static function criarConexao(): void
    {
        try {
            self::$conexao = new \PDO(self::$dsn, self::$username, self::$password, self::$options);
        } catch (\PDOException $e) {
            self::errorConexao($e);
        }
    }

    public static function errorConexao(\PDOException $error): void
    {
        throw new \InvalidArgumentException("Error conexão:\n" . $error->getMessage(), 1);
    }
}
