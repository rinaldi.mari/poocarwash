<?php

namespace config\security;

abstract class UsuarioLogado
{
    private static \PDO $connection;

    public static function verificarSessao(): void
    {
        if (!isset($_SESSION)) {
            session_start();
        }
    }

    public static function verificarUsuario(): bool
    {
        $result = false;
        $dadosUsuario = self::getUsuario();

        if ($dadosUsuario) {
            self::setEmailSessao($dadosUsuario["EMAIL"]);
            $result = true;
        }

        return $result;
    }

    public static function getUsuario()
    {
        $stmt = self::$connection->prepare("SELECT ID, EMAIL FROM poocarwash.pessoa WHERE EMAIL = :EMAIL");
        $stmt->bindValue(":EMAIL", self::getEmailSessao(), \PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetch();
    }

    private static function getEmailSessao(): string
    {
        return empty($_SESSION["email"]) ? "" : $_SESSION["email"];
    }

    private static function setEmailSessao(string $email): void
    {
        $_SESSION["emailSessao"] = trim($email);
    }

    public static function setConnection(\PDO $connection): void
    {
        self::$connection = $connection;
    }
}
