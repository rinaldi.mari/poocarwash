<?php

namespace abstracts\ORM;

use abstracts\ORM\AUXCRUD;

class CRUD extends AUXCRUD
{
    private string $query;
    private string $conditions = " WHERE 1=1 ";
    private string $joinComand = "";
    private string $order = "";
    private array $bindsValues = [];
    private string $typrQuery = "";

    public function create(): bool
    {
        $this->setTypeQuery("create");
        $this->cleanBinds();
        $this->cleanConditions();

        $columns = $this->getColumns();
        $columnsBind = $this->getColumnsBind();

        $this->query = "INSERT INTO {$this->dbName}{$this->tableName} ({$columns}) VALUES ({$columnsBind})";
        $stmt = $this->pdo->prepare($this->query);

        return $stmt->execute($this->getValuesArray());
    }

    public function find(string $columns): array
    {
        $this->setTypeQuery("find");

        $this->query = "SELECT {$columns} FROM {$this->dbName}{$this->tableName} ";
        return $this->pdo->query($this->query)->fetchAll();
    }

    public function findById(string $columns, int $id)
    {
        $this->setTypeQuery("findById");

        $this->query = "SELECT {$columns} FROM {$this->dbName}{$this->tableName} WHERE ID = :ID";

        $stmt = $this->pdo->prepare($this->query);
        $stmt->execute([":ID" => $id]);

        return $stmt->fetch();
    }

    public function select(string $columns, string $nickName = ""): self
    {
        $this->setTypeQuery("select");

        $this->query = "SELECT {$columns} FROM {$this->dbName}{$this->tableName} {$nickName} ";
        return $this;
    }

    public function update(string $columns = ""): self
    {
        $this->setTypeQuery("update");
        $this->cleanBinds();
        $this->cleanConditions();

        $columns = empty($columns) ? $this->getColumnsUpdate() : $columns;

        $this->query = "UPDATE {$this->dbName}{$this->tableName} SET {$columns} ";

        return $this;
    }

    public function delete(): self
    {
        $this->setTypeQuery("delete");
        $this->cleanBinds();
        $this->cleanConditions();

        $this->query = "DELETE FROM {$this->dbName}{$this->tableName} ";

        return $this;
    }

    public function where(string $conditions): self
    {
        $this->conditions .= $conditions;
        return $this;
    }

    public function binds(array $bindsArray): self
    {
        if ($this->checkTypeQuery()) {
            $this->bindsValues = array_merge($this->bindsValues, $this->getValuesArray());
        }

        $this->bindsValues = array_merge($this->bindsValues, $bindsArray);

        return $this;
    }

    public function join(string $typeJoin, string $tableName, string $conditions): self
    {
        $this->joinComand .= " {$typeJoin} JOIN {$tableName} ON ($conditions) ";
        return $this;
    }

    public function orderBy(string $columns, string $order = "ASC"): self
    {
        $this->order .= " {$columns} {$order} ";
        return $this;
    }

    public function execute(): bool
    {
        $stmt = $this->pdo->prepare($this->getFullQuery());
        return empty($this->bindsValues) ? $stmt->execute() : $stmt->execute($this->bindsValues);
    }

    public function getFullQuery(): string
    {
        return "{$this->query} {$this->joinComand} {$this->conditions} {$this->order}";
    }

    public function fetch(int $fetchMethod = \PDO::FETCH_ASSOC)
    {
        return $this->getfetchs("fetch", $fetchMethod);
    }

    public function fetchAll(int $fetchMethod = \PDO::FETCH_ASSOC)
    {
        return $this->getfetchs("fetchAll", $fetchMethod);
    }

    private function getfetchs(string $fetch, int $fetchMethod)
    {
        $data = "";

        $stmt = $this->pdo->prepare($this->getFullQuery());
        empty($this->bindsValues) ? $stmt->execute() : $stmt->execute($this->bindsValues);

        if ($fetch == "fetch") {
            $data = $stmt->fetch($fetchMethod);
        } else {
            $data = $stmt->fetchAll($fetchMethod);
        }
        return $data;
    }

    private function setTypeQuery(string $typrQuery): void
    {
        $this->typrQuery = $typrQuery;
    }

    private function checkTypeQuery(): bool
    {
        return !in_array($this->typrQuery, ["delete"]);
    }

    private function cleanBinds(): void
    {
        $this->bindsValues = [];
    }

    private function cleanConditions(): void
    {
        $this->conditions = " WHERE 1=1 ";
    }
}
