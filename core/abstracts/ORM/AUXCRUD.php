<?php

namespace abstracts\ORM;

use abstracts\ModelAbstract;

abstract class AUXCRUD extends ModelAbstract
{
    protected string $tableName;
    protected array $columns;
    protected string $dbName;

    protected function __construct(string $tableName, array $columns, string $dbName = "poocarwash.")
    {
        $this->setTableName($tableName);
        $this->setColumns($columns);
        $this->setDBName($dbName);

        parent::__construct();
    }

    public function setAllData(array $dados = null): void
    {
        $array = $dados ?? $_REQUEST;

        foreach ($array as $chave => $valor) {
            $method = "set" . $this->verificaSnackCase($chave);
            if ($chave != "acao" && method_exists($this, $method)) {
                try {
                    $this->$method($valor);
                } catch (\Throwable $th) {
                    throw new \InvalidArgumentException("Método não encontrado", 1);
                }
            }
        }
    }

    private function setTableName(string $tableName): void
    {
        $this->tableName = $tableName;
    }

    private function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }

    private function setDBName(string $dbName): void
    {
        $this->dbName = $dbName;
    }

    protected function getColumnsBind(): string
    {
        $columnsJoin = array_slice($this->columns, 1, count($this->columns));
        return ":" . join(", :", $columnsJoin);
    }

    protected function getColumns(): string
    {
        $columnsJoin = array_slice($this->columns, 1, count($this->columns));
        return join(", ", $columnsJoin);
    }

    protected function getColumnsUpdate(): string
    {
        $columnsUpdate = array();

        foreach ($this->columns as $column) {
            if ($column != "ID") {
                $columnsUpdate[] = "{$column} = :{$column}";
            }
        }
        return join(", ", $columnsUpdate);
    }

    protected function getValuesArray(): array
    {
        $valuesArray = [];

        foreach ($this->columns as $column) {
            $method = $this->callGetMethod($column);

            if ($column != "ID" && !empty($method)) {
                $valuesArray[":{$column}"] = $this->callGetMethod($column);
            }
        }

        return $valuesArray;
    }

    private function callGetMethod(string $column)
    {
        $strPeace = $this->verificaSnackCase($column);

        $column = "get" . $strPeace;
        return $this->$column();
    }

    private function verificaSnackCase(string $column): string
    {
        $strPeace = "";

        if (count(explode("_", $column)) > 1) {
            foreach (explode("_", $column) as $value) {
                $strPeace .= ucfirst(strtolower($value));
            }
        } else {
            $strPeace = ucfirst(strtolower($column));
        }

        return $strPeace;
    }
}
