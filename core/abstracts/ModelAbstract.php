<?php

namespace abstracts;

require_once($_SERVER["DOCUMENT_ROOT"] . "/core/autoload/autoload.php");

use interfaces\IConnection;
use config\connection\MySQLConnection;

class ModelAbstract
{
    protected \PDO $pdo;
    protected string $sql = "";
    protected array $parametros = [];

    public function __construct(IConnection $conexao = NULL)
    {
        if ($conexao === NULL) {
            $this->pdo = MySQLConnection::getInstance();
        } else {
            $this->pdo = $conexao::getInstance();
        }
    }
}
