<?php

namespace abstracts;

require_once($_SERVER["DOCUMENT_ROOT"] . "/core/autoload/autoload.php");

abstract class ControllerAbstract
{
    public static function init(): object
    {
        $classe = get_called_class();
        return new $classe();
    }

    protected function __construct()
    {
        $this->echoDataAcao();
    }

    private function echoDataAcao(): void
    {
        echo $this->ajustarRetorno($this->getDataAcao($this->getAcao()));
    }

    private function getAcao(): string
    {
        return array_key_exists("acao", $_REQUEST) ? trim($_REQUEST["acao"]) : "";
    }

    private function getDataAcao(string $acao)
    {
        return $this->$acao();
    }

    private function ajustarRetorno($dados): string
    {
        return ($dados === true || $dados === false) ? $this->toJSON(["resultado" => $dados]) : $this->toJSON($dados);
    }

    private function toJSON($dados): string
    {
        return json_encode($dados);
    }
}
