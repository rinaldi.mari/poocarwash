<?php

namespace interfaces;

interface IConnection
{
    public static function getInstance(): \PDO;
    public static function errorConexao(\PDOException $error): void;
}