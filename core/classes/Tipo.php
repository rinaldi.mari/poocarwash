<?php

namespace classes;

class Tipo
{
    public const CLIENTE = 1;
    public const GERENTE = 2;
    public const FUNCIONARIO = 3;

    public function getTipoName(int $tipo): string
    {
        $tipoName = "";

        switch ($tipo) {
            case 2:
                $tipoName = "Gerente";
                break;
            case 3:
                $tipoName = "Funcionário";
                break;
            default:
                $tipoName = "Cliente";
                break;
        }

        return $tipoName;
    }
}
