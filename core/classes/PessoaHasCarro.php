<?php

namespace classes;

use abstracts\ORM\CRUD;

class PessoaHasCarro extends CRUD
{
    private int $id;
    private int $pessoaId;
    private int $carroId;

    public const ID = "ID";
    public const PESSOA_ID = "PESSOA_ID";
    public const CARRO_ID = "CARRO_ID";

    public function __construct()
    {
        parent::__construct("pessoa_has_carro", [self::ID, self::PESSOA_ID, self::CARRO_ID]);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getPessoaId(): int
    {
        return $this->pessoaId;
    }

    public function setPessoaId(int $pessoaId): void
    {
        $this->pessoaId = $pessoaId;
    }

    public function getCarroId(): int
    {
        return $this->carroId;
    }

    public function setCarroId(int $carroId): void
    {
        $this->carroId = $carroId;
    }
}
