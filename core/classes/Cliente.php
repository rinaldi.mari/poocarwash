<?php

namespace classes;

require_once($_SERVER["DOCUMENT_ROOT"] . "/core/autoload/autoload.php");

use classes\Pessoa;
use classes\Carro;

class Cliente extends Pessoa
{
  private string $carro;

  public const CARRO = "CARRO";

  public function __construct()
  {
    parent::__construct("pessoa", [
      self::ID,
      self::NOME,
      self::TELEFONE,
      self::EMAIL,
      self::TIPO,
      self::SALARIO
    ]);
  }

  public function getCarro(): string
  {
    return $this->carro;
  }

  public function setCarro(string $carro): void
  {
    $this->carro = $carro;
  }
}
