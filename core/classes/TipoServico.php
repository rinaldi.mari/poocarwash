<?php

namespace classes;

use abstracts\ORM\CRUD;

class TipoServico extends CRUD
{
    private int $id;
    private float $valor;
    private string $descricao;

    public const ID = "ID";
    public const VALOR = "VALOR";
    public const DESCRICAO = "DESCRICAO";

    public function __construct()
    {
        parent::__construct("tipo_servico", [self::ID, self::VALOR, self::DESCRICAO]);
    }

    public function getValor(): float
    {
        return $this->valor;
    }

    public function setValor(float $valor): void
    {
        $this->valor = $valor;
    }

    public function getDescricao(): string
    {
        return $this->descricao;
    }

    public function setDescricao($descricao): void
    {
        $this->descricao = $descricao;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }
}
