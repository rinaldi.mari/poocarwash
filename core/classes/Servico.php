<?php

namespace classes;

use abstracts\ORM\CRUD;
use DateTime;

class Servico extends CRUD
{
    private int $id = 0;
    private string $dataHora = "";
    private int $lavador = 0;
    private int $gerente = 0;
    private int $tipoServicoId = 0;
    private int $statusServicoId = 0;
    private int $carroId = 0;

    public const ID = "ID";
    public const DATA_HORA = "DATA_HORA";
    public const LAVADOR = "LAVADOR";
    public const GERENTE = "GERENTE";
    public const TIPO_SERVICO_ID = "TIPO_SERVICO_ID";
    public const STATUS_SERVICO_ID = "STATUS_SERVICO_ID";
    public const CARRO_ID = "CARRO_ID";

    public function __construct()
    {
        parent::__construct("servico", [
            self::ID,
            self::DATA_HORA,
            self::LAVADOR,
            self::GERENTE,
            self::TIPO_SERVICO_ID,
            self::STATUS_SERVICO_ID,
            self::CARRO_ID
        ]);
    }

    public function getDataHora(): string
    {
        return $this->dataHora;
    }

    public function setDataHora(string $dataHora): void
    {
        $this->dataHora = $dataHora;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getCarroId(): int
    {
        return $this->carroId;
    }

    public function setCarroId(int $carroId): void
    {
        $this->carroId = $carroId;
    }

    public function getGerente(): string
    {
        return $this->gerente;
    }

    public function setgerente(string $gerente): void
    {
        $this->gerente = $gerente;
    }

    public function getTipoServicoId(): int
    {
        return $this->tipoServicoId;
    }

    public function setTipoServicoId(int $tipoServicoId): void
    {
        $this->tipoServicoId = $tipoServicoId;
    }

    public function getStatusServicoId(): int
    {
        return $this->statusServicoId;
    }

    public function setStatusServicoId(int $statusServicoId): void
    {
        $this->statusServicoId = $statusServicoId;
    }

    public function getLavador(): int
    {
        return $this->lavador;
    }

    public function setLavador(int $lavador): void
    {
        $this->lavador = $lavador;
    }
}
