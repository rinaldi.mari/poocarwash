<?php

namespace classes;

use abstracts\ORM\CRUD;

class StatusServico extends CRUD
{
    private int $id;
    private string $descricao;

    public const ID = "ID";
    public const DESCRICAO = "DESCRICAO";

    public function __construct()
    {
        parent::__construct("status_servico", [self::ID, self::DESCRICAO]);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getDescricao(): string
    {
        return $this->descricao;
    }

    public function setDescricao(string $descricao)
    {
        $this->descricao = $descricao;
    }
}
