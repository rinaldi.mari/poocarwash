<?php

namespace classes;

require_once($_SERVER["DOCUMENT_ROOT"] . "/core/autoload/autoload.php");

use classes\Pessoa;

class Funcionario extends Pessoa
{
  private float $salario;
  private string $tipo;

  public const SALARIO = "SALARIO";
  public const TIPO = "TIPO";

  public function __construct()
  {
    parent::__construct("pessoa", [
      self::ID,
      self::NOME,
      self::TELEFONE,
      self::EMAIL,
      self::SALARIO,
      self::TIPO
    ]);
  }

  public function getSalario(): float
  {
    return $this->salario;
  }

  public function getTipo(): string
  {
    return $this->tipo;
  }

  public function setSalario(float $salario): void
  {
    $this->salario = $salario;
  }

  public function setTipo(string $tipo): void
  {
    $this->tipo = $tipo;
  }
}
