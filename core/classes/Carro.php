<?php

namespace classes;

require_once($_SERVER["DOCUMENT_ROOT"] . "/core/autoload/autoload.php");

use abstracts\ORM\CRUD;

class Carro extends CRUD
{
  private int $id = 0;
  private string $placa = "";
  private string $cor = "";
  private string $marca = "";
  private string $modelo = "";

  public const ID = "ID";
  public const PLACA = "PLACA";
  public const COR = "COR";
  public const MARCA = "MARCA";
  public const MODELO = "MODELO";


  public function __construct()
  {
    parent::__construct("carro", [selF::ID, self::PLACA, self::COR, self::MARCA, self::MODELO]);
  }

  public function getPlaca(): string
  {
    return $this->placa;
  }
  public function setPlaca(string $placa): void
  {
    $this->placa = $placa;
  }

  public function getCor(): string
  {
    return $this->cor;
  }

  public function setCor(string $cor): void
  {
    $this->cor = $cor;
  }

  public function getMarca(): string
  {
    return $this->marca;
  }

  public function setMarca(string $marca): void
  {
    $this->marca = $marca;
  }

  public function getModelo(): string
  {
    return $this->modelo;
  }

  public function setModelo(string $modelo): void
  {
    $this->modelo = $modelo;
  }

  public function getId(): int
  {
    return $this->id;
  }

  public function setId(int $id): void
  {
    $this->id = $id;
  }
}
