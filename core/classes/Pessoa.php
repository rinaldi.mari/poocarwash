<?php

namespace classes;

use abstracts\ORM\CRUD;

class Pessoa extends CRUD
{
  private int $id = 0;
  private string $nome = "";
  private int $telefone = 0;
  private string $email = "";
  private string $tipo = "";
  private float $salario = 0.0;

  public const ID = "ID";
  public const NOME = "NOME";
  public const TELEFONE = "TELEFONE";
  public const EMAIL = "EMAIL";
  public const TIPO = "TIPO";
  public const SALARIO = "SALARIO";

  public function __construct(string $tableName = "pessoa", array $columns = [self::ID, self::NOME, self::TELEFONE, self::EMAIL, self::TIPO, self::SALARIO])
  {
    parent::__construct($tableName, $columns);
  }

  public function getNome(): string
  {
    return $this->nome;
  }

  public function getTelefone(): int
  {
    return $this->telefone;
  }

  public function getEmail(): string
  {
    return $this->email;
  }

  public function setNome(string $nome): void
  {
    $this->nome = $nome;
  }

  public function setTelefone(int $telefone): void
  {
    $this->telefone = $telefone;
  }

  public function setEmail(string $email): void
  {
    $this->email = $email;
  }

  public function getId(): int
  {
    return $this->id;
  }

  public function setId(int $id): void
  {
    $this->id = $id;
  }

  public function getTipo(): string
  {
    return $this->tipo;
  }

  public function setTipo(string $tipo): void
  {
    $this->tipo = $tipo;
  }

  public function getSalario(): float
  {
    return $this->salario;
  }

  public function setSalario(float $salario): void
  {
    $this->salario = $salario;
  }
}
