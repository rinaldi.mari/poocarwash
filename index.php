<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/core/autoload/autoload.php");

use classes\Servico;
use config\security\UsuarioLogado;

function irParaPagina()
{

    UsuarioLogado::verificarSessao();
    UsuarioLogado::setConnection(config\connection\MySQLConnection::getInstance());

    // $pagina = "pages/login/login.php";

    // if (UsuarioLogado::verificarUsuario()) {
    $pagina = "pages/home/home_page.php";
    // }

    (new Servico)->create();

    require_once($pagina);
}


?>


<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Trabalho de Poo">
    <meta name="author" content="Creative Tim">
    <title>Lava Jato</title>

    <link rel="icon" href="assets/img/brand/favicon.png" type="image/png">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">

    <link rel="stylesheet" href="assets/vendor/nucleo/css/nucleo.css" type="text/css">
    <link rel="stylesheet" href="assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">

    <link rel="stylesheet" href="assets/css/main.css" type="text/css">
    <link rel="stylesheet" href="assets/css/argon.css?v=1.2.0" type="text/css">
    <link rel="stylesheet" href="assets/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" type="text/css">

    <script src="assets/vendor/jquery/dist/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/js-cookie/js.cookie.js"></script>
    <script src="assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
    <script src="assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>

    <script src="assets/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="assets/vendor/chart.js/dist/Chart.extension.js"></script>

    <script src="assets/js/argon.js?v=1.2.0"></script>
    <script src="assets/js/jquery/jquery-3.5.1.min.js"></script>
    <script src="assets/js/components/vendor/bootstrap-datepicker.min.js"></script>

    <script src="assets/js/main.js"></script>
</head>

<body>
    <div id="main-default">
        <?= irParaPagina(); ?>
    </div>
</body>

</html>