-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema poocarwash
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema poocarwash
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `poocarwash` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `poocarwash` ;

-- -----------------------------------------------------
-- Table `poocarwash`.`carro`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `poocarwash`.`carro` ;

CREATE TABLE IF NOT EXISTS `poocarwash`.`carro` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `placa` VARCHAR(7) NOT NULL ,
  `cor` VARCHAR(10) NOT NULL ,
  `marca` VARCHAR(45) NOT NULL ,
  `modelo` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`)  )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `poocarwash`.`tipo_servico`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `poocarwash`.`tipo_servico` ;

CREATE TABLE IF NOT EXISTS `poocarwash`.`tipo_servico` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `descricao` VARCHAR(45) NOT NULL ,
  `valor` DOUBLE(10,2) NOT NULL ,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `poocarwash`.`pessoa`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `poocarwash`.`pessoa` ;

CREATE TABLE IF NOT EXISTS `poocarwash`.`pessoa` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `nome` VARCHAR(45) NOT NULL ,
  `telefone` VARCHAR(15) NOT NULL ,
  `email` VARCHAR(100) NOT NULL ,
  `tipo` VARCHAR(10) NOT NULL ,
  `salario` DOUBLE(10,2) NULL ,
  PRIMARY KEY (`id`)  )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `poocarwash`.`status_servico`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `poocarwash`.`status_servico` ;

CREATE TABLE IF NOT EXISTS `poocarwash`.`status_servico` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `descricao` VARCHAR(10) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `poocarwash`.`servico`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `poocarwash`.`servico` ;

CREATE TABLE IF NOT EXISTS `poocarwash`.`servico` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `data_hora` DATETIME NOT NULL ,
  `lavador` INT NOT NULL ,
  `gerente` INT NOT NULL ,
  `tipo_servico_id` INT NOT NULL ,
  `status_servico_id` INT NOT NULL ,
  `carro_id` INT NOT NULL ,
  PRIMARY KEY (`id`)  ,
  CONSTRAINT `fk_servico_pessoa1`
    FOREIGN KEY (`lavador`)
    REFERENCES `poocarwash`.`pessoa` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_servico_pessoa2`
    FOREIGN KEY (`gerente`)
    REFERENCES `poocarwash`.`pessoa` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_servico_tipo_servico1`
    FOREIGN KEY (`tipo_servico_id`)
    REFERENCES `poocarwash`.`tipo_servico` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_servico_status_servico1`
    FOREIGN KEY (`status_servico_id`)
    REFERENCES `poocarwash`.`status_servico` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_servico_carro1`
    FOREIGN KEY (`carro_id`)
    REFERENCES `poocarwash`.`carro` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_servico_pessoa1_idx` ON `poocarwash`.`servico` (`lavador` ASC)  ;

CREATE INDEX `fk_servico_pessoa2_idx` ON `poocarwash`.`servico` (`gerente` ASC)  ;

CREATE INDEX `fk_servico_tipo_servico1_idx` ON `poocarwash`.`servico` (`tipo_servico_id` ASC)  ;

CREATE INDEX `fk_servico_status_servico1_idx` ON `poocarwash`.`servico` (`status_servico_id` ASC)  ;

CREATE INDEX `fk_servico_carro1_idx` ON `poocarwash`.`servico` (`carro_id` ASC)  ;


-- -----------------------------------------------------
-- Table `poocarwash`.`pessoa_has_carro`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `poocarwash`.`pessoa_has_carro` ;

CREATE TABLE IF NOT EXISTS `poocarwash`.`pessoa_has_carro` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `pessoa_id` INT NOT NULL ,
  `carro_id` INT NOT NULL ,
  PRIMARY KEY (`id`)  ,
  CONSTRAINT `fk_pessoa_has_carro_pessoa`
    FOREIGN KEY (`pessoa_id`)
    REFERENCES `poocarwash`.`pessoa` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pessoa_has_carro_carro1`
    FOREIGN KEY (`carro_id`)
    REFERENCES `poocarwash`.`carro` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_pessoa_has_carro_carro1_idx` ON `poocarwash`.`pessoa_has_carro` (`carro_id` ASC)  ;

CREATE INDEX `fk_pessoa_has_carro_pessoa_idx` ON `poocarwash`.`pessoa_has_carro` (`pessoa_id` ASC)  ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
