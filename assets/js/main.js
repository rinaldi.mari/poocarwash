$(document).ready(function (e) {
    const mainId = "#container-fluid"

    if (window.location.hash) {
        let path = window.location.hash.substring(1)
        $(mainId).load(ajustarPath(path))
    }

    jQuery(window).on("hashchange", function (e) {
        let path = window.location.hash.substring(1)
        $(mainId).load(ajustarPath(path))
    })

    function ajustarPath(path) {
        return "pages/" + path + ".php"
    }

    $(".nav-link").click(function (e) {
        trocaPagina($(this).attr("href"))
    })

    function trocaPagina(path) {
        window.location.hash = path
    }
})
